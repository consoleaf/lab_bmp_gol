#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include "qdbmp.h"
#include "qdbmp.c"
#include <stdbool.h>
#include <string.h>
#include <getopt.h>
#include <math.h>

UCHAR ** arr_r, ** new_arr_r;
BMP * bmp;
BMP * newbmp;
UCHAR r, g, b;
UINT width, height;
UINT x, y;
char * input;
char * output;
int max_iter = -1, dump_freq = 1;

UCHAR computeColor(int x, int y) {
    int count = 0;

    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            // printf("%d %d\n", y + dy, x + dx);
            if ((dy == 0) && (dx == 0) || (x + dx < 0) || (x + dx >= width) || (y + dy < 0) || (y + dy >= height))
                continue;

            count += (arr_r[y + dy][x + dx] == 255 ? 1 : 0);
        }
    }

    // fprintf(stderr, "%d\n", count);

    if (arr_r[y][x] == 255 && (count == 2 || count == 3))
        return 255;
    if (arr_r[y][x] == 0 && count == 3)
        return 255;
    return 0;
}

void doNewStage(char * filename) {
    for (x = 0; x < width; ++x) {
        for (y = 0; y < height; ++y) {

            r = computeColor(x, y);

            new_arr_r[y][x] = r;

            BMP_SetPixelRGB(newbmp, x, y, r, r, r);

        }
    }
    return;
}

int main(int argc, char * argv[]) {
    static
    const struct option longopts[] = {
        {
            .name = "input", .has_arg = required_argument, .val = 'i'
        },
        {
            .name = "output",
            .has_arg = required_argument,
            .val = 'o'
        },
        {
            .name = "max_iter",
            .has_arg = required_argument,
            .val = 'm'
        },
        {
            .name = "dump_freq",
            .has_arg = required_argument,
            .val = 'd'
        },
        {},
    };

    char * input;
    char * output;
    int max_iter = -1, dump_freq = 1;

    for (;;) {
        int opt = getopt_long(argc, argv, "bf", longopts, NULL);
        if (opt == -1)
            break;
        switch (opt) {
        case 'i':
            // fprintf(stderr, "Got --input: %s\n", optarg);
            input = optarg;
            break;
        case 'o':
            // fprintf(stderr, "Got --output: %s\n", optarg);
            output = optarg;
            break;
        case 'm':
            // fprintf(stderr, "Got --max_iter: %s\n", optarg);
            max_iter = atoi(optarg);
            break;
        case 'd':
            // fprintf(stderr, "Got --dump_freq: %s\n", optarg);
            dump_freq = atoi(optarg);
            break;
        default:
            /* Unexpected option */
            return 1;
        }
    }

    if (!input || !output) {
        fprintf(stderr, "Incorrect arguments\n");
        return -1;
    }

    bmp = BMP_ReadFile(input);

    BMP_CHECK_ERROR(stderr, -1); /* If an error has occurred, notify and exit */

    /* Get image's dimensions */
    width = BMP_GetWidth(bmp);
    height = BMP_GetHeight(bmp);

    arr_r = (UCHAR ** ) malloc(height * sizeof(UCHAR * ));
    for (int i = 0; i < height; i++)
        arr_r[i] = (UCHAR * ) malloc(width * sizeof(UCHAR));

    new_arr_r = (UCHAR ** ) malloc(height * sizeof(UCHAR * ));
    for (int i = 0; i < height; i++)
        new_arr_r[i] = (UCHAR * ) malloc(width * sizeof(UCHAR));

    /* Iterate through all the image's pixels */

    for (x = 0; x < width; ++x) {
        for (y = 0; y < height; ++y) {
            /* Get pixel's RGB values */
            BMP_GetPixelRGB(bmp, x, y, & r, & g, & b);

            arr_r[y][x] = r;
        }
    }

    BMP_Free(bmp);

    if (max_iter < 0)
        max_iter = 1000;

    for (size_t i = 0; i < max_iter; i++) {
        char newname[100];
        sprintf(newname, "%s%d.bmp", output, i);

        newbmp = BMP_Create(width, height, 24);

        doNewStage(newname);

        if (i % dump_freq == 0) {
        /* Save result */
        BMP_WriteFile(newbmp, newname);
        BMP_CHECK_ERROR(stderr, -2);
        }

        /* Free all memory allocated for the image */
        BMP_Free(newbmp);

        for (size_t y = 0; y < height; y++) {
            for (size_t x = 0; x < width; x++) {
                arr_r[y][x] = new_arr_r[y][x];
            }
        }
    }

    return 0;
}
